import React from 'react';
import { formatNumber } from './utils';

export function ProductRows({products, productOrder}) {
    return products
        .sort(sortProducts)
        .map(({name, revenue}) => (
            <tr key={name}>
                <td>{name}</td>
                <td>{formatNumber(revenue)}</td>
            </tr>
        ))

        function sortProducts(a, b) {
            if(productOrder === 'asc') {
                if(a.name < b.name) { return -1; }
                if(a.name > b.name) { return 1; }
            }

            if(productOrder === 'desc') {
                if(a.name < b.name) { return 1; }
                if(a.name > b.name) { return -1; }
            }
            
            return 0;
        }
}