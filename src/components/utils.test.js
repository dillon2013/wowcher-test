import { buildProductList, calculateRevenueTotal } from "./utils";

describe('utils', () => {

  let data;
  let productList;

  beforeEach(() => {
      data = [
          {
              "id": "000",
              "name": "Bok Choy",
              "unitPrice": 13.63,
              "sold": 100
            },
            {
              "id": "001",
              "name": "Bok Choy",
              "unitPrice": 10.78,
              "sold": 200
            },
            {
              "id": "002",
              "name": "Bok Choy",
              "unitPrice": 7.34,
              "sold": 300
            },
            {
              "id": "003",
              "name": "Casaba Melon",
              "unitPrice": 6.71,
              "sold": 100
            },
            {
              "id": "004",
              "name": "Casaba Melon",
              "unitPrice": 12.99,
              "sold": 200
            },
            {
              "id": "004",
              "name": "Iceberg Lettuce",
              "unitPrice": 12.99,
              "sold": 100
            },
      ];

      productList = buildProductList(data);
  })

    describe('buildProductList', () => {
        it('should remove duplicates from the list of products', () => {
            expect(productList.length).toEqual(3)
        })

        it('should add up the sold values of duplicates', () => {
            expect(productList[0].sold).toEqual(600)
        })

        it('should add up the unitPrice values of duplicates', () => {
            expect(productList[0].unitPrice).toEqual(31.75)
        })

        it('should add a revenue property on the object', () => {
          expect(productList[0].revenue).toEqual(19050)
          expect(productList[1].revenue).toEqual(5910)
          expect(productList[2].revenue).toEqual(1299)
        })
    })

    describe('calculateRevenueTotal', () => {
      it('should calculate the total sum of revenue', () => {
        expect(calculateRevenueTotal(productList)).toEqual(26259)
      })
    })
})