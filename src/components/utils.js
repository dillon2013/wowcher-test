export {
    formatNumber,
    buildProductList,
    calculateRevenueTotal
} 

function  formatNumber(number) {
    return new Intl.NumberFormat("en", { minimumFractionDigits: 2 }).format(number);
}

function buildProductList(arr) {
    let unique = {};

    arr.forEach(({name, unitPrice, sold}) => {
        const productInUnqueObj = unique[name];
        if(productInUnqueObj) {
            productInUnqueObj.unitPrice = productInUnqueObj.unitPrice + unitPrice;
            productInUnqueObj.sold = productInUnqueObj.sold + sold;
        } else {
            unique[name] = {name, unitPrice, sold}
        }

        unique[name].revenue = unique[name].unitPrice * unique[name].sold;
        
    })

    return Object.values(unique)
}

function calculateRevenueTotal(products) {
    return products.reduce((acc, curr) => acc + curr.revenue, 0)
}

