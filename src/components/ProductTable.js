import React from 'react';
import { formatNumber, buildProductList, calculateRevenueTotal } from './utils';
import { ProductRows } from './ProductRows';

export class ProductTable extends React.Component {

    state = {
        products: null,
        filteredProducts: null,
        productOrder: 'asc',
        totalRevenue: 0,
        searchInput: ''
    }

    async componentDidMount() {
        try {
            const values = await Promise.all([
                fetch('api/branch1.json'),
                fetch('api/branch2.json'),
                fetch('api/branch3.json')
            ])
            const data = await Promise.all(values.map(branch => branch.json()));
            let products = [
                ...data[0].products, 
                ...data[1].products, 
                ...data[2].products
            ];

            const generatedProductsList = buildProductList(products);
            this.setState({
                products: generatedProductsList,
                filteredProducts: generatedProductsList,
                totalRevenue: calculateRevenueTotal(generatedProductsList)
            })

        } catch (error) {
            console.log(error)
        }
    }

    handleInputChange = (e) => {
        const {value} = e.target;

        const filteredProducts = this.state.products.filter(product => {
            return  product.name.toLowerCase().includes(value.toLowerCase())
        })

        this.setState({
            searchInput: value,
            filteredProducts,
            totalRevenue: calculateRevenueTotal(filteredProducts)
        })
    }

    toggleSortOrder = () => {
        const {productOrder} = this.state;
        const order = productOrder === 'asc' ? 'desc' : 'asc';
        this.setState({productOrder: order})
    }

    render() {
        const {filteredProducts, searchInput, productOrder, totalRevenue } = this.state;
        return (
            !filteredProducts ? <p>Loading...</p>
            : (
                <div className="product-list">
                    <label className="productList__label">Search Products</label>
                    <input name="searchInput" type="text" onChange={this.handleInputChange} value={searchInput} />
                        
                    <table>
                        <thead>
                        <tr>
                            <th onClick={this.toggleSortOrder}>Product</th>
                            <th>Revenue</th>
                        </tr>
                        </thead>
                        <tbody>
                            <ProductRows 
                                productOrder={productOrder} 
                                products={filteredProducts} 
                            />
                        </tbody>
                        <tfoot>
                        <tr>
                            <td>Total</td>
                            <td>{formatNumber(totalRevenue)}</td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            ) 
        )
    }
}
